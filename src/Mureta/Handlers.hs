{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE NamedFieldPuns    #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE TypeOperators     #-}

module Mureta.Handlers where

import           Control.Concurrent
import           Control.Monad              (void)
import           Control.Monad.IO.Class     (liftIO)
import           Control.Monad.Trans.Reader (ask)
import           Data.Aeson                 (Result (..),
                                             Value (Object, String), fromJSON,
                                             toJSON)
import           Data.Aeson.QQ
import qualified Data.HashMap.Strict        as H
import           Data.IORef                 (atomicModifyIORef', readIORef)
import qualified Data.Map                   as M
import           Data.UUID
import           Mureta.Types
import           Network.Wreq               (post)
import           Servant
import           System.Random              (randomIO)

matrizToResult :: UUID -> String -> Value
matrizToResult uuid "ocr_rg_rf" =
  [aesonQQ|
  {
    "result": {
      "atualizado_em": "2018-09-14T16:15:35.123Z",
      "mensagem": "Válido.",
      "nome": "ocr_rg_rf",
      "numero": #{uuid},
      "parametros": {
        "cpf": "64501382627",
        "data_de_expedicao_rg": "10/09/2013",
        "data_de_nascimento": "06/12/1973",
        "digito_rg": "X",
        "estado_expedicao_rg": "SP",
        "nome": "FULANO DA SILVA",
        "nome_da_mae": "MARIA DA SILVA",
        "nome_do_pai": "JOAO DA SILVA",
        "orgao_emissor_rg": "SSP",
        "rg": "123456"
      },
      "resultado": "VALID",
      "status": "CONCLUIDO",
      "documento_ocr": {
        "numero": null,
        "filename_front": "idwall-ocr-cnhs.s3.amazonaws.com/rg_e2810ece-d0d0-43bb-932a-89bcb0be3dc1e7db08c5-2ca6-4e0c-9780-4ae33e478738_front.png",
        "filename_back": "idwall-ocr-cnhs.s3.amazonaws.com/rg_e2810ece-d0d0-43bb-932a-89bcb0be3dc1e7db08c5-2ca6-4e0c-9780-4ae33e478738_back.png",
        "status": "OK",
        "rg": "123456X",
        "data_de_expedicao": "10/09/2013",
        "nome": "FULANO DA SILVA",
        "nome_do_pai": "JOAO DA SILVA",
        "nome_da_mae": "MARIA DA SILVA",
        "naturalidade": "S. PAULO - SP",
        "data_de_nascimento": "06/12/1973",
        "doc_origem": "SÃO PAULO - SP",
        "cpf": "64501382627",
        "orgao_emissor": "SSP",
        "estado_emissao": "SP",
        "id_usuario": "704",
        "matriz": "ocr_rg_rf",
        "id_protocolo": "5744432",
        "filename_full": null,
        "backoffice_num": "052c063582e15adebd45b2ff150f6250",
        "mask": false
      }
    },
    "status_code": 200
  }
  |]
matrizToResult uuid "ocr_cnh_rf" =
  [aesonQQ|
  {
    "result": {
      "atualizado_em": "2018-09-14T17:18:12.203Z",
      "mensagem": "Válido.",
      "nome": "ocr_cnh_rf",
      "numero": #{uuid},
      "parametros": {
        "cnh": "123456789",
        "cnh_data_da_primeira_habilitacao": "23/01/1992",
        "cnh_data_de_validade": "08/12/2022",
        "cnh_estado_expedicao": "SP",
        "cnh_numero_espelho": "987654321",
        "cnh_numero_renach": "SP000000000",
        "cpf": "64501382627",
        "data_de_nascimento": "06/12/1973",
        "estado_expedicao_cnh": "SP",
        "estado_expedicao_rg": "SP",
        "nome": "FULANO DA SILVA",
        "nome_da_mae": "MARIA DA SILVA",
        "nome_do_pai": "JOAO DA SILVA",
        "orgao_emissor_rg": "SSP",
        "rg": "123456",
        "seguranca_cnh": "00000000000"
      },
      "resultado": "VALID",
      "status": "CONCLUIDO",
      "documento_ocr": {
        "filename_front": null,
        "id_usuario": "704",
        "status": "OK",
        "nome": "FULANO DA SILVA",
        "rg": "123456",
        "orgao_emissor_rg": "SSP",
        "estado_emissao_rg": "SP",
        "cpf": "64501382627",
        "data_de_nascimento": "06/12/1973",
        "nome_do_pai": "JOAO DA SILVA",
        "nome_da_mae": "MARIA DA SILVA",
        "categoria": "B",
        "numero_registro": "00000000000",
        "validade": "08/12/2022",
        "data_primeira_habilitacao": "23/01/1992",
        "observacoes": "A",
        "data_de_emissao": "08/12/2017",
        "numero_renach": "SP000000000",
        "numero_espelho": "987654321",
        "permissao": "",
        "orgao_emissor": "DETRAN",
        "local_emissao": "SAO PAULO",
        "estado_emissao": "SP",
        "acc": "",
        "numero": "15607475fb008f6b0ba0673b840d3a93",
        "numero_seguranca": "00000000000",
        "filename_back": null,
        "matriz": "validaCnhDefault",
        "id_protocolo": "5745168",
        "foto_perfil": null,
        "rg_digito": "",
        "filename_full": "idwall-ocr-cnhs.s3.amazonaws.com/cnh_14f8a4d4-7819-405f-b267-ffc2ac1dc5ff3338eaf2-fcc4-4ab5-a602-6882f97c4ed5_full.jpeg",
        "mask": false
      }
    },
    "status_code": 200
  }
  |]
matrizToResult _ m = error $ "matriz desconhecida: " ++ m

server :: ServerT MuretaAPI AppM
server = novoRel :<|> getParams
  where
    novoRel :: RelatorioReq -> AppM RelatorioResponse
    novoRel RelatorioReq {matriz} = do
      State {reports, webhookUrl, timeout} <- ask
      uid <-
        liftIO $ do
          uid <- randomIO
          let ocrData = matrizToResult uid matriz
          atomicModifyIORef' reports (\m -> (M.insert uid ocrData m, ()))
          forkIO $ fireWebhook timeout webhookUrl uid
          pure uid
      pure
        RelatorioResponse
          { status_code = 200
          , result = Object $ H.singleton "numero" (String (toText uid))
          }
    getParams :: UUID -> AppM RelatorioResponse
    getParams uid = do
      State {reports} <- ask
      rs <- liftIO $ readIORef reports
      case M.lookup uid rs of
        Just r -> do
          let Success result = fromJSON r
          pure result
        Nothing -> throwError err404

fireWebhook :: Int -> String -> UUID -> IO ()
fireWebhook timeout url uid = do
  threadDelay timeout
  void $
    post url $
    toJSON $
    H.fromList
      [ ("tipo" :: String, String "protocolo_status")
      , ( "dados"
        , Object $
          H.fromList
            [("protocolo", String $ toText uid), ("status", String "CONCLUIDO")])
      ]

app :: State -> Application
app s = serve api $ hoistServer api (nt s) server
